package com.google.gwt.sample.client;

import java.io.IOException;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String addUserInfoToList(String userName, String userPassword);
	String chekingIfUserExist(String userName, String userPassword);

	Guitar newGuitarInList(Guitar guitar) throws IOException;
	String deleteGuitarFromList(String guitarName, String guitarColor, String numberOfStrings) throws IOException;
	String updateGuitarInfoInsideTheList(Guitar oldGuitarInfo, Guitar newGuitarInfo) throws IOException;
}
