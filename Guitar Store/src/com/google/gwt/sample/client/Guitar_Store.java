package com.google.gwt.sample.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Guitar_Store implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
//	private static final String SERVER_ERROR = "An error occurred while "
//			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	
	private MenuBar menu = new MenuBar();
	
	RegisterPage regPage = new RegisterPage();
	UserPage userPage = new UserPage();
	BasketPage basketPage = new BasketPage();
	GuitarTablePage guitarTablePage = new GuitarTablePage();
	LoginPage loginPage = new LoginPage();

	public void onModuleLoad() {
		
		loginPage.showLoginPage();
		
		//navigation bar
		menu.setAutoOpen(true);
		menu.setAnimationEnabled(true);
		
		MenuBar fileMenu = new MenuBar(true);
	    fileMenu.setAnimationEnabled(true);

		menu.addItem("Guitar Table", new Command() {

			@Override
			public void execute() {
				regPage.hideRegisterPage();
				userPage.hideUserPage();
				basketPage.hide();
				guitarTablePage.hideAdminTable();
				guitarTablePage.hideTable();
	
				guitarTablePage.showTable();
				loginPage.hideLoginPage();
				RootPanel.get("login").setVisible(false);
			}
			
		});
		menu.addSeparator();
		menu.addItem("Login page", new Command() {
			@Override
			public void execute() {
				regPage.hideRegisterPage();
				userPage.hideUserPage();
				basketPage.hide();
				guitarTablePage.hideTable();
				guitarTablePage.hideAdminTable();
				
				loginPage.showLoginPage();
				RootPanel.get("login").setVisible(true);
			}
		});
		menu.addSeparator();
		menu.addItem("User Page", new Command() {
			@Override
			public void execute() {
				userPage.hideUserPage();
				regPage.hideRegisterPage();
				userPage.showUserPage();
				basketPage.hide();
				guitarTablePage.hideTable();
				loginPage.hideLoginPage();
				RootPanel.get("login").setVisible(false);
			}
		});
		menu.addSeparator();
		menu.addItem("Basket", new Command() {
			@Override
			public void execute() {
				basketPage.hide();
				regPage.hideRegisterPage();
				userPage.hideUserPage();
				basketPage.show();
				guitarTablePage.hideTable();
				loginPage.hideLoginPage();
				RootPanel.get("login").setVisible(false);
			}
		});
		RootPanel.get("menuContainer").add(menu);
		
	}
		
}
