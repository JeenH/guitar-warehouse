package com.google.gwt.sample.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LoginPage {
	
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	User userInfo = new User("admin", "admin", Role.ADMIN);
	
	private VerticalPanel loginPanel = new VerticalPanel();
	private HorizontalPanel loginHPanel1 = new HorizontalPanel();
	private HorizontalPanel loginHPanel2 = new HorizontalPanel();
	private HorizontalPanel loginHpanel3 = new HorizontalPanel();
	private HorizontalPanel loginHpanel4 = new HorizontalPanel();
	private Label loginLabel = new Label("Login:");
	private Label passwordLabel = new Label("Password:");
	private Button signButton = new Button("Log In");
	private TextBox nameField = new TextBox();
	private PasswordTextBox passwordField = new PasswordTextBox();
	private Label errorLabel = new Label();	
	private Label newUserLabel = new Label("Don't have account?");
	private Label newUserButton = new Label("Create");	

	RegisterPage regPage = new RegisterPage();
	UserPage userPage = new UserPage();
	BasketPage basketPage = new BasketPage();
	GuitarTablePage guitarTablePage = new GuitarTablePage();

	public LoginPage() {
		loadLoginPage();
	}
	
	private void loadLoginPage() {
		
		//login page
		loginHPanel1.add(loginLabel);
		loginHPanel1.add(nameField);
		loginHPanel2.add(passwordLabel);
		loginHPanel2.add(passwordField);
		loginHPanel2.add(signButton);
		loginHpanel3.add(errorLabel);
		loginHpanel4.add(newUserLabel);
		loginHpanel4.add(newUserButton);
		
		loginPanel.add(loginHPanel1);
		loginPanel.add(loginHPanel2);
		loginPanel.add(loginHpanel3);
		loginPanel.add(loginHpanel4);
		loginPanel.setSpacing(7);
					
		//login stuff
				
		class SignInButtonHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				checkIfUserExist(nameField.getText(), passwordField.getText());
			}
			public void checkIfUserExist(String login, String password) {
				greetingService.chekingIfUserExist(login, password, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}

					@Override
					public void onSuccess(String result) {
						if (result.equalsIgnoreCase("Welcome")) {
							RootPanel.get("login").setVisible(false);
							loginPanel.setVisible(false);
							if (nameField.getText().equals("admin")) {
								userInfo.setRole(Role.ADMIN);
								guitarTablePage.showAdminTable();
							} 
							else {
								guitarTablePage.showTable();						
								userInfo.setRole(Role.USER);
							}
						} 
						else {
							errorLabel.setText(result);
						}
					}
				});
			}
			
		}
		SignInButtonHandler signInButtonHandler = new SignInButtonHandler();
		signButton.addClickHandler(signInButtonHandler);
		
		newUserButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				loginPanel.setVisible(false);
				regPage.showRegPage();
			}
		});

		//added style
		errorLabel.addStyleName("errorLabel");
		newUserButton.addStyleName("newUserButton");
		loginPanel.addStyleName("loginPanel");
	}
	public void hideLoginPage() {
		RootPanel.get().remove(loginPanel);
	}
	public void showLoginPage() {
		RootPanel.get().add(loginPanel);
	}
}
