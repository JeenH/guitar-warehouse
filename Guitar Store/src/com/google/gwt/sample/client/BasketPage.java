package com.google.gwt.sample.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class BasketPage {
	private Label pageName = new Label();
	private VerticalPanel basketPanel = new VerticalPanel();
	private Button buyAllBasketButton = new Button("Buy all");
	
	public BasketPage() {
		loadBasketPage();
	}
	
	private void loadBasketPage() {
		buyAllBasketButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				basketPanel.clear();
			}
			
		});
		basketPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		pageName.setText("Basket");
		basketPanel.add(pageName);
		
		basketPanel.add(buyAllBasketButton);
		basketPanel.setWidth("100%");
		basketPanel.addStyleName("basketPanel");
	}
	
	public void addWidget(VerticalPanel newPanel) {
		basketPanel.add(newPanel);
	}
	public void hide() {
		RootPanel.get().remove(basketPanel);
	}
	public void show() {
		RootPanel.get().add(basketPanel);
	}
}
