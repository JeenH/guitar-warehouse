package com.google.gwt.sample.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GuitarTablePage {
	
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private VerticalPanel userPanel = new VerticalPanel();

	private Button openMainPanel = new Button("Add Guitar");
	private FlexTable guitarTable = new FlexTable();
	private FlexTable userTable = new FlexTable();
	private HorizontalPanel addHPanel1 = new HorizontalPanel();
	private HorizontalPanel addHPanel2 = new HorizontalPanel();
	private TextBox guitarNameField = new TextBox();
	private TextBox quantityField = new TextBox();
	private TextBox priceField = new TextBox();
	private RadioButton electroButton = new RadioButton("type", "electric");
	private RadioButton acousticButton = new RadioButton("type", "acoustic");
	private Button addButton = new Button("Add");
	private Button saveButton = new Button("Save");
	private Button canselButton = new Button("Cancel");
	private ListBox numberOfStringsListBox = new ListBox();
	private ListBox colorsListBox = new ListBox();
	private Label numberOfGuitarStringsLabel = new Label("Number of guitar strings:");
	private Label colorLabel = new Label("Color:");
	private Label guitarNameLabel = new Label("Name:");
	private Label quantityLabel = new Label("Qantity:");
	private Label typeLabel = new Label("Type:");
	private Label priceLabel = new Label("Price:");
	
	private Button openFilter = new Button("Open Filter Menu");
	private Button closeFilter = new Button("Close Filter Menu");
	private Label filterLabel = new Label("Filter by:");
	private VerticalPanel filterPanel = new VerticalPanel();
	private HorizontalPanel filterHPanel1 = new HorizontalPanel();
	private HorizontalPanel filterHPanel2 = new HorizontalPanel();
	private TextBox filterNameField = new TextBox();
	private TextBox filterQuantityField = new TextBox();
	private ListBox filterColorListBox = new ListBox();
	private RadioButton filterElectroButton = new RadioButton("filterType", "electric");
	private RadioButton filterAcousticButton = new RadioButton("filterType", "acoustic");
	private ListBox filterStringsListBox = new ListBox();
	private TextBox filterPriceField = new TextBox();
	private Label filterPriceLabel = new Label("Price:");
	private Label filterNameLabel = new Label("Name:");
	private Label filterColorLabel = new Label("Color:");
	private Label filterTypeLabel = new Label("Type:");
	private Label filterQuantityLabel = new Label("Quantity:");
	private Label filterStringsLabel = new Label("Strings:");
	private Button filterNameButton = new Button("Find");
	private Button filterColorButton = new Button("Find");
	private Button filterTypeButton = new Button("Find");
	private Button filterQuantityButton = new Button("Find");
	private Button filterStringsButton = new Button("Find");
	private Button filterPriceButton = new Button("Find");
	private Button showAllTableButton = new Button("Show all");

	BasketPage basketPage = new BasketPage();
	RegisterPage regPage = new RegisterPage();
	UserPage userPage = new UserPage();
	LoginPage loginPage = new LoginPage();
	
	private int tableIndexForSaveButton = 1;
	User userInfo = new User("admin", "admin", Role.ADMIN);
	private Guitar oldGuitar = new Guitar();
	private Guitar newGuitar = new Guitar();
	Guitar guitar = new Guitar();
	
	public static TableRowElement findNearestParentRow(Node node) {
		Node element = findNearestParentNodeByType(node, "tr");
		if (element != null) {
			return element.cast();
		}
		return null;
	}
	public static Node findNearestParentNodeByType(Node node, String nodeType) {
		while ((node != null)) {
			if (Element.is(node)) {
				Element elem = Element.as(node);

				String tagName = elem.getTagName();

				if (nodeType.equalsIgnoreCase(tagName)) {
					return elem.cast();
				}

			}
			node = node.getParentNode();
		}
		return null;
	}
	
	public GuitarTablePage() {
		loadUserGuitarTable();
		loadAdminGuitarTable();
	}
	
	private void loadUserGuitarTable() {
		
		filterPanel.addStyleName("filterPanel");
		filterPanel.add(closeFilter);		
		filterPanel.add(filterHPanel1);
		filterPanel.setWidth("100%");
		filterPanel.add(filterHPanel2);
		filterHPanel1.setWidth("100%");
		filterHPanel2.setWidth("100%");
		
		//filter panel
				filterStringsListBox.addItem("");
				filterStringsListBox.addItem("6");
				filterStringsListBox.addItem("3");
				filterStringsListBox.addItem("4");
				filterStringsListBox.addItem("12");
				filterStringsListBox.addItem("more");

				filterColorListBox.addItem("");
				filterColorListBox.addItem("white");
				filterColorListBox.addItem("red");
				filterColorListBox.addItem("beige");
				filterColorListBox.addItem("black");

				filterHPanel1.add(filterLabel);
				
				filterHPanel1.add(filterNameLabel);
				filterHPanel1.add(filterNameField);
				filterHPanel1.add(filterNameButton);
				filterHPanel1.add(filterColorLabel);
				filterHPanel1.add(filterColorListBox);
				filterHPanel1.add(filterColorButton);
				
				filterHPanel2.add(filterQuantityLabel);
				filterHPanel2.add(filterQuantityField);
				filterHPanel2.add(filterQuantityButton);
				filterHPanel2.add(filterTypeLabel);
				filterHPanel2.add(filterElectroButton);
				filterHPanel2.add(filterAcousticButton);
				filterHPanel2.add(filterTypeButton);
				
				filterHPanel1.add(filterStringsLabel);
				filterHPanel1.add(filterStringsListBox);
				filterHPanel1.add(filterStringsButton);
				filterHPanel1.add(showAllTableButton);
				
				filterHPanel2.add(filterPriceLabel);
				filterHPanel2.add(filterPriceField);
				filterHPanel2.add(filterPriceButton);
				filterPanel.setSpacing(7);
				
				filterPanel.setVisible(false);
				closeFilter.setVisible(false);
				
				openFilter.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						openFilter.setVisible(false);
						closeFilter.setVisible(true);
						filterPanel.setVisible(true);
						
					}
					
				});
				closeFilter.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						openFilter.setVisible(true);
						closeFilter.setVisible(false);
						filterPanel.setVisible(false);
					}
					
				});
				
				filterNameButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						for (int i = 1; i <= userTable.getRowCount(); i++) {
							if (!filterNameField.getText().equals("")
									&& !userTable.getText(i, 0).equals(filterNameField.getText())) {
							
								userTable.getRowFormatter().setVisible(i, false);
							}
						}
						filterNameField.setText("");
					}
				});

				filterColorButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						for (int i = 1; i <= userTable.getRowCount(); i++) {
							if (!filterColorListBox.getSelectedItemText().equals("")
									&& !userTable.getText(i, 1).equals(filterColorListBox.getSelectedItemText())) {
								userTable.getRowFormatter().setVisible(i, false);
							}
						}
					}
				});

				filterQuantityButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						for (int i = 1; i <= userTable.getRowCount(); i++) {
							if (!filterQuantityField.getText().equals("")
									&& !userTable.getText(i, 2).equals(filterQuantityField.getText())) {
								userTable.getRowFormatter().setVisible(i, false);
							}
						}
						filterQuantityField.setText("");
					}
				});

				filterTypeButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						String filterGuitarType;
						if (filterElectroButton.getValue()) {
							filterGuitarType = "electric";
						} 
						else if (filterAcousticButton.getValue()) {
							filterGuitarType = "acoustic";
						} 
						else {
							filterGuitarType = "";
						}
						for (int i = 1; i <= userTable.getRowCount(); i++) {
							if (!userTable.getText(i, 3).equals(filterGuitarType)) {
								userTable.getRowFormatter().setVisible(i, false);
							}
						}
						filterElectroButton.setValue(false);
						filterAcousticButton.setValue(false);
					}
				});

				filterStringsButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						for (int i = 1; i <= userTable.getRowCount(); i++) {
							if (!filterStringsListBox.getSelectedItemText().equals("") 
									&& !userTable.getText(i, 4)
									.equals(filterStringsListBox.getSelectedItemText())) {
								userTable.getRowFormatter().setVisible(i, false);
							}
						}
					}
				});
				
				filterPriceButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						for (int i = 1; i <= userTable.getRowCount(); i++) {
							if (!filterPriceField.getText().equals("") && 
									!userTable.getText(i, 5).equals(filterPriceField.getText())) {
								userTable.getRowFormatter().setVisible(i, false);
							}
						}
					}
					
				});

				showAllTableButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						if (userTable.getRowCount() > 1) {
							for (int i = 1; i <= userTable.getRowCount(); i++) {
								if (!userTable.getRowFormatter().isVisible(i)) {
									userTable.getRowFormatter().setVisible(i, true);
								}
							}
						}
					}
				});
		// guitar table panel
		numberOfStringsListBox.addItem("6");
		numberOfStringsListBox.addItem("3");
		numberOfStringsListBox.addItem("4");
		numberOfStringsListBox.addItem("12");
		numberOfStringsListBox.addItem("more");
		
		colorsListBox.addItem("white");
		colorsListBox.addItem("red");
		colorsListBox.addItem("beige");		
		colorsListBox.addItem("black");

		electroButton.setValue(true);
		
		//user panel
		userTable.setText(0, 0, "Name");
		userTable.setText(0, 1, "Color");
		userTable.setText(0, 2, "Quantity");
		userTable.setText(0, 3, "Type");
		userTable.setText(0, 4, "Srings");
		userTable.setText(0, 5, "Price");
		userTable.setText(0, 6, "Buy");
		userTable.setCellPadding(7);
		userTable.setBorderWidth(0);
		
		userPanel.add(openMainPanel);
		userPanel.add(openFilter);
		userPanel.add(userTable);
		userPanel.setWidth("100%");
		
		userPanel.setStyleName("userName");
		userTable.setStyleName("userTable");
		userTable.getRowFormatter().addStyleName(0, "guitarListHeader");
		
		openFilter.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				openFilter.setVisible(false);
				closeFilter.setVisible(true);
				filterPanel.setVisible(true);
			}
			
		});
		openMainPanel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (userInfo.getRole().equals(Role.ADMIN)) {
					showAdminTable();
					userPanel.setVisible(false);
					mainPanel.setVisible(true);
				} 
				else {
					Window.alert("You are not allowed to do it");
					return;
				}
			}
		});

	}
	public void loadAdminGuitarTable () {
		
			//mainPanel stuff
			class AddButtonHandler implements ClickHandler  {
				@Override
				public void onClick(ClickEvent event) {
					if (!guitarNameField.getText().matches("^[0-9a-zA-Z\\.]{1,30}$")) {
						Window.alert("'" + guitarNameField.getText() + "' is not a valid symbol.");
						guitarNameField.selectAll();
						return;
					}
					if (!quantityField.getText().matches("^[1-9]{1,2}$")) {
						Window.alert("Quantity field is numeric field, it should contain a number from 1 to 99");
						quantityField.selectAll();
						return;
					}
					guitar.setName(guitarNameField.getText());
					guitar.setQuantity(quantityField.getText());
					guitar.setPrice(priceField.getText());
					guitar.setColor(colorsListBox.getSelectedItemText());
					guitar.setStrings(numberOfStringsListBox.getSelectedItemText());
					if (electroButton.getValue()) {
						guitar.setType("electro");
					} 
					else {
						guitar.setType("acoustic");
					}
					sendGuitarToServer();
					guitarNameField.setText("");
					quantityField.setText("");
					
				}	
				public void sendGuitarToServer() {
					greetingService.newGuitarInList(guitar, new AsyncCallback<Guitar>() {
						@Override
						public void onFailure(Throwable caught) {						
						}
						@Override
						public void onSuccess(Guitar result) {
							int row = guitarTable.getRowCount();
							int row2 = userTable.getRowCount();
							Button deleteButton = new Button("Delete");
							Button editButton = new Button("Edit");
							final Button buyButton = new Button("Buy");

							guitarTable.setText(row, 0, result.getName());
							guitarTable.setText(row, 1, result.getColor());
							guitarTable.setText(row, 2, result.getQuantity());
							guitarTable.setText(row, 3, result.getType());
							guitarTable.setText(row, 4, result.getStrings());
							guitarTable.setText(row, 5, result.getPrice());
							guitarTable.setWidget(row, 6, editButton);
							guitarTable.setWidget(row, 7, deleteButton);
							
							userTable.setText(row2, 0, result.getName());
							userTable.setText(row2, 1, result.getColor());
							userTable.setText(row2, 2, result.getQuantity());
							userTable.setText(row2, 3, result.getType());
							userTable.setText(row2, 4, result.getStrings());
							userTable.setText(row2, 5, result.getPrice());
							userTable.setWidget(row2, 6, buyButton);
							
							//confirmation DialogBox
							final DialogBox dialogBox = new DialogBox();
							dialogBox.setText("Confirmation");
							dialogBox.setAnimationEnabled(true);
							final Button confirmBuyButton = new Button("Confirm");
							final Button cancelBuyButton = new Button("Cancel");
							final Label dialogText = new Label("You sure you want to buy:");
							final Label selectedGuitar = new Label();
							final Label dialogPrice = new Label();
							final TextBox dialogQuantity = new TextBox();
							VerticalPanel dialogPanel = new VerticalPanel();
							dialogPanel.add(dialogText);
							dialogPanel.add(selectedGuitar);
							dialogPanel.add(dialogPrice);
							dialogPanel.add(dialogQuantity);
							dialogPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
							dialogPanel.add(confirmBuyButton);
							dialogPanel.add(cancelBuyButton);
							dialogPanel.setSpacing(7);
							dialogBox.setWidget(dialogPanel);
							
							buyButton.addClickHandler(new ClickHandler() {	
							
								@Override
								public void onClick(ClickEvent event) {
									if (!userInfo.getRole().equals(Role.USER))
									{
										Window.alert("You should sign in or register to buy guitar");
										return;
									}
									dialogBox.center();
									TableRowElement tableRow = findNearestParentRow(buyButton.getElement());
									selectedGuitar.setText(userTable.getText(tableRow.getRowIndex(), 0));
									dialogPrice.setText("by Price '" + userTable.getText(tableRow.getRowIndex(), 5) + "$' in quantity of");
									Integer quantity = Integer.parseInt(userTable.getText(tableRow.getRowIndex(), 2));
									dialogQuantity.setText(quantity.toString());
									confirmBuyButton.setFocus(true);
								}
								
							});
							cancelBuyButton.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									dialogBox.hide();
								}
								
							});
							confirmBuyButton.addClickHandler(new ClickHandler() {		
								
								@Override
								public void onClick(ClickEvent event) {
									TableRowElement tableRow = findNearestParentRow(buyButton.getElement());
									oldGuitar.setName(userTable.getText(tableRow.getRowIndex(), 0));
									oldGuitar.setColor(userTable.getText(tableRow.getRowIndex(), 1));
									oldGuitar.setQuantity(userTable.getText(tableRow.getRowIndex(), 2));
									oldGuitar.setType(userTable.getText(tableRow.getRowIndex(), 3));
									oldGuitar.setStrings(userTable.getText(tableRow.getRowIndex(), 4));
									oldGuitar.setPrice(userTable.getText(tableRow.getRowIndex(), 5));
									
									Integer oldQuantity = Integer.parseInt(userTable.getText(tableRow.getRowIndex(), 2));
									Integer newQuantity = Integer.parseInt(dialogQuantity.getText());
									if (!quantityField.getText().matches("^[1-9]{1,2}$") && oldQuantity < newQuantity) {
										Window.alert(
												"Quantity should contain a number from 1 to " + userTable.getText(tableRow.getRowIndex(), 2));
										return;
									}
									Label newGuitarQuantity = new Label("Price: " + userTable.getText(tableRow.getRowIndex(), 5) + "$");
									Label newGuitarLabel = new Label("Guitar: " + userTable.getText(tableRow.getRowIndex(), 0));
									VerticalPanel newGuitarInBusket = new VerticalPanel();
									newGuitarInBusket.addStyleName("newGuitarInBusket");
									newGuitarInBusket.add(newGuitarLabel);
									newGuitarInBusket.add(newGuitarQuantity);
									basketPage.addWidget(newGuitarInBusket);
									userTable.setText(tableRow.getRowIndex(), 2, String.valueOf(oldQuantity - newQuantity));
									newGuitar.setName(userTable.getText(tableRow.getRowIndex(), 0));
									newGuitar.setColor(userTable.getText(tableRow.getRowIndex(), 1));
									newGuitar.setQuantity(userTable.getText(tableRow.getRowIndex(), 2));
									newGuitar.setType(userTable.getText(tableRow.getRowIndex(), 3));
									newGuitar.setStrings(userTable.getText(tableRow.getRowIndex(), 4));
									newGuitar.setPrice(userTable.getText(tableRow.getRowIndex(), 5));
									
									Integer newQuantityValue = Integer.parseInt(userTable.getText(tableRow.getRowIndex(), 2));
									
									if (newQuantityValue < 1) {
										guitarTable.removeRow(tableRow.getRowIndex());
										userTable.removeRow(tableRow.getRowIndex());
										deleteGuitarInfoFromServer(userTable.getText(tableRow.getRowIndex(), 0), userTable.getText(tableRow.getRowIndex(), 1), 
												userTable.getText(tableRow.getRowIndex(), 4));
									}
									udateGuitarInfoOnServer(oldGuitar,newGuitar);
									
									dialogBox.hide();
									
									final DialogBox okBox = new DialogBox();
									okBox.setAnimationEnabled(true);
									final Label okLabel = new Label("New Guitar Added to Basket");
									final Button okBoxButton = new Button("Check Basket");
									VerticalPanel okBoxPanel = new VerticalPanel();
									okBoxPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
									HorizontalPanel okBoxHPanel1 = new HorizontalPanel();
									HorizontalPanel okBoxHPanel2 = new HorizontalPanel();
									okBoxButton.addClickHandler(new ClickHandler() {

										@Override
										public void onClick(ClickEvent event) {
											regPage.hideRegisterPage();
											userPage.hideUserPage();
											basketPage = new BasketPage();
											userPanel.setVisible(false);
											mainPanel.setVisible(false);
											loginPage.hideLoginPage();
											RootPanel.get("login").setVisible(false);
											okBox.hide();
										}
										
									});
									okBoxHPanel1.add(okLabel);
									okBoxHPanel2.add(okBoxButton);
									okBoxPanel.add(okBoxHPanel1);
									okBoxPanel.add(okBoxHPanel2);
									okBox.setWidget(okBoxPanel);
									
									okBox.center();
								}
								
							});
							
							deleteButton.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent event) {
									TableRowElement tableRow = findNearestParentRow(deleteButton.getElement());
									guitarTable.removeRow(tableRow.getRowIndex());
									userTable.removeRow(tableRow.getRowIndex());
									deleteGuitarInfoFromServer(result.getName(), result.getColor(),
											result.getStrings());
								}
							});
							editButton.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									TableRowElement tableRow = findNearestParentRow(editButton.getElement());
									tableIndexForSaveButton = tableRow.getRowIndex();
									guitarNameField.setText(guitarTable.getText(tableRow.getRowIndex(), 0));
										oldGuitar.setName(guitarNameField.getText());
									quantityField.setText(guitarTable.getText(tableRow.getRowIndex(), 2));
										oldGuitar.setQuantity(quantityField.getText());
									priceField.setText(guitarTable.getText(tableRow.getRowIndex(), 5));
									if (guitarTable.getText(tableRow.getRowIndex(), 3).equalsIgnoreCase("electro")) {
										oldGuitar.setType("electro");
										electroButton.setEnabled(true);
									} 
									else {
										oldGuitar.setType("acoustic");
										acousticButton.setEnabled(true);
									}
									if (guitarTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("white")) {
										colorsListBox.setItemSelected(0, true);
										oldGuitar.setColor("white");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("red")) {
										colorsListBox.setItemSelected(1, true);
										oldGuitar.setColor("red");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("black")) {
										colorsListBox.setItemSelected(2, true);
										oldGuitar.setColor("black");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 1).equalsIgnoreCase("beige")) {
										colorsListBox.setItemSelected(3, true);
										oldGuitar.setColor("beige");
									}
									
									if (guitarTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("6")) {
										numberOfStringsListBox.setItemSelected(0, true);
										oldGuitar.setStrings("6");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("3")) {
										numberOfStringsListBox.setItemSelected(1, true);
										oldGuitar.setStrings("3");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("4")) {
										numberOfStringsListBox.setItemSelected(2, true);
										oldGuitar.setStrings("4");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("12")) {
										numberOfStringsListBox.setItemSelected(3, true);
										oldGuitar.setStrings("12");
									} 
									else if (guitarTable.getText(tableRow.getRowIndex(), 4).equalsIgnoreCase("more")) {
										numberOfStringsListBox.setItemSelected(4, true);
										oldGuitar.setStrings("more");
									}
									addButton.setVisible(false);
									saveButton.setVisible(true);
									canselButton.setVisible(true);
									
									canselButton.addClickHandler(new ClickHandler() {

										@Override
										public void onClick(ClickEvent event) {
											addButton.setVisible(true);
											saveButton.setVisible(false);
											canselButton.setVisible(false);										
										}
									});
									saveButton.addClickHandler(new ClickHandler() {

										@Override
										public void onClick(ClickEvent event) {
											if (!guitarNameField.getText().matches("^[0-9a-zA-Z\\.]{1,30}$")) {
												Window.alert("'" + guitarNameField.getText() + "' is not a valid symbol.");
												return;
											}
											if (!quantityField.getText().matches("^[1-9]{1,2}$")) {
												Window.alert(
														"Quantity field is numeric field, it should contain a number from 1 to 99");
												return;
											}
											if (!priceField.getText().matches("^[1-9]{1,2}$")) {
												Window.alert(
														"Price field is numeric field, it should contain a number from 1 to 99");
												return;
											}
											guitarTable.setText(tableIndexForSaveButton, 0, guitarNameField.getText());
											userTable.setText(tableIndexForSaveButton, 0, guitarNameField.getText());
											newGuitar.setName(guitarNameField.getText());
											guitarTable.setText(tableIndexForSaveButton, 1,
													colorsListBox.getSelectedItemText());
											userTable.setText(tableIndexForSaveButton, 1,
													colorsListBox.getSelectedItemText());
											guitarTable.setText(tableIndexForSaveButton, 5, priceField.getText());
											userTable.setText(tableIndexForSaveButton, 5, priceField.getText());
											newGuitar.setPrice(priceField.getText());
											newGuitar.setColor(colorsListBox.getSelectedItemText());
											guitarTable.setText(tableIndexForSaveButton, 2, quantityField.getText());
											userTable.setText(tableIndexForSaveButton, 2, quantityField.getText());
											newGuitar.setQuantity(quantityField.getText());
											guitarTable.setText(tableIndexForSaveButton, 4,
													numberOfStringsListBox.getSelectedItemText());
											userTable.setText(tableIndexForSaveButton, 4,
													numberOfStringsListBox.getSelectedItemText());
											newGuitar.setStrings(numberOfStringsListBox.getSelectedItemText());
											if (electroButton.getValue()) {
												guitarTable.setText(tableIndexForSaveButton, 3, "electric");
												userTable.setText(tableIndexForSaveButton, 3, "electric");
												newGuitar.setType("electric");
											} 
											else {
												guitarTable.setText(tableIndexForSaveButton, 3, "acoustic");
												userTable.setText(tableIndexForSaveButton, 3, "acoustic");
												newGuitar.setType("acoustic");
											}
											udateGuitarInfoOnServer(oldGuitar, newGuitar);
											addButton.setVisible(true);
											saveButton.setVisible(false);
											canselButton.setVisible(false);
											guitarNameField.setText("");
											quantityField.setText("");
										}
										
									});
								}
							});
					}
				});
				
			}
				public void deleteGuitarInfoFromServer(String guitarName, String guitarColor,
						String numberOfGuitarStrings) {

					greetingService.deleteGuitarFromList(guitarName, guitarColor, numberOfGuitarStrings,
							new AsyncCallback<String>() {
						
								@Override
								public void onSuccess(String result) {
									Window.alert("Guitar info successfully deleted from the server!");
								}

								@Override
								public void onFailure(Throwable caught) {
									Window.alert(caught.toString());
								}
							});
				}
				
				public void udateGuitarInfoOnServer(Guitar oldGuitar, Guitar newGuitar) {
					greetingService.updateGuitarInfoInsideTheList(oldGuitar, newGuitar, new AsyncCallback<String>() {

								@Override
								public void onSuccess(String result) {
									Window.alert("Guitar info successfully updated on the server!");
								}

								@Override
								public void onFailure(Throwable caught) {
									Window.alert(caught.toString());
								}
							});
				}

			}

			
			AddButtonHandler addButtonHandler = new AddButtonHandler();
			addButton.addClickHandler(addButtonHandler);
			
		
		mainPanel.setStyleName("mainPanel");
		addHPanel1.setStyleName("hPanel1");
		addHPanel2.setStyleName("hPanel2");
		guitarTable.setStyleName("guitarList");		
		guitarTable.getRowFormatter().addStyleName(0, "guitarListHeader");
		//adding guitar panel
		addHPanel1.add(guitarNameLabel);
		addHPanel1.add(guitarNameField);
		addHPanel1.add(priceLabel);
		addHPanel1.add(priceField);
		addHPanel1.add(typeLabel);
		addHPanel1.add(electroButton);
		addHPanel1.add(acousticButton);
		addHPanel1.add(addButton);
		addHPanel1.add(saveButton);
		addHPanel1.add(canselButton);

		addHPanel2.add(quantityLabel);
		addHPanel2.add(quantityField);
		addHPanel2.add(numberOfGuitarStringsLabel);
		addHPanel2.add(numberOfStringsListBox);
		addHPanel2.add(colorLabel);
		addHPanel2.add(colorsListBox);
		addHPanel2.setSpacing(7);
		
		saveButton.setVisible(false);
		canselButton.setVisible(false);

		
		//guitar Table panel
		guitarTable.setText(0, 0, "Name");
		guitarTable.setText(0, 1, "Color");
		guitarTable.setText(0, 2, "Quantity");
		guitarTable.setText(0, 3, "Type");
		guitarTable.setText(0, 4, "Srings");
		guitarTable.setText(0, 5, "Price");
		guitarTable.setCellPadding(5);
		guitarTable.setBorderWidth(0);
		
		
		mainPanel.add(addHPanel1);
		mainPanel.add(addHPanel2);
		mainPanel.add(guitarTable);
		mainPanel.setSpacing(7);
		mainPanel.setWidth("100%");

	}

	public void hideTable() {
		RootPanel.get().remove(userPanel);
		RootPanel.get().remove(filterPanel);
	}
	public void hideAdminTable() {
		RootPanel.get().remove(mainPanel);
	}
	public void showAdminTable() {
		RootPanel.get().add(mainPanel);
	}
	public void showTable() {
		RootPanel.get().add(filterPanel);
		RootPanel.get().add(userPanel);
	}
}
