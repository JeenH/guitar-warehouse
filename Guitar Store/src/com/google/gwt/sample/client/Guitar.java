package com.google.gwt.sample.client;

import java.io.Serializable;

public class Guitar implements Serializable {

	private String name;
	private String price;
	private String quantity;
	private String type;
	private String strings;
	private String color;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStrings() {
		return strings;
	}
	public void setStrings(String strings) {
		this.strings = strings;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Guitar(String name, String price, String quantity, String type, String strings, String color) {
		super();
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.type = type;
		this.strings = strings;
		this.color = color;
	}
	public Guitar() {
	}
	
}
