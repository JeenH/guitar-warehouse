package com.google.gwt.sample.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void addUserInfoToList(String userName, String userPassword, AsyncCallback<String> callback);
	void chekingIfUserExist(String userName, String userPassword, AsyncCallback<String> callback);
	
	void newGuitarInList(Guitar guitar, AsyncCallback<Guitar> callback);
	void deleteGuitarFromList(String guitarName, String guitarColor, String numberOfGuitarStrings, AsyncCallback<String> callback);
	void updateGuitarInfoInsideTheList(Guitar oldGuitarInfo, Guitar newGuitarInfo, AsyncCallback<String> callback);

}
