package com.google.gwt.sample.client;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class UserPage {
	User userInfo = new User();
	private VerticalPanel userPagePanel = new VerticalPanel();
	private Label userPageLabel = new Label("Greetings From Guitar Warehouse Developers!");
	private Label userPageText = new Label();
	private Button changeName = new Button("Change Name");
	private Button changePassword = new Button("Change Password");
	private HorizontalPanel userPageHPanel1 = new HorizontalPanel();
	private HorizontalPanel userPageHPanel2 = new HorizontalPanel();
	
	public UserPage() {
		loadUserPage();
	}
	
	private void loadUserPage() {
		//userPage
		userPageHPanel1.add(changeName);
		userPageHPanel2.add(changePassword);
		userPagePanel.add(userPageLabel);
		userPageText.setText("Your username is '" + userInfo.getLogin() +
				"'. We hope you will have a great time serfing our site");
		userPagePanel.add(userPageText);
		userPagePanel.add(userPageHPanel1);
		userPagePanel.add(userPageHPanel2);

		userPagePanel.addStyleName("userPagePanel");
		userPageLabel.addStyleName("userPageLabel");
		userPageText.addStyleName("userPageText");
		userPageHPanel1.addStyleName("userPageHPanel");
		userPageHPanel2.addStyleName("userPageHPanel");
	}
	public void hideUserPage() {
		RootPanel.get().remove(userPagePanel);
	}
	public void showUserPage() {
		RootPanel.get().add(userPagePanel);
	}
}
