package com.google.gwt.sample.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RegisterPage {
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	private HorizontalPanel regHPanel1 = new HorizontalPanel();
	private HorizontalPanel regHpanel2 = new HorizontalPanel();
	private HorizontalPanel regHpanel3 = new HorizontalPanel();
	private VerticalPanel regPanel = new VerticalPanel();
	private Label errorRegLabel = new Label();
	private Label regNameLabel = new Label("Login:");
	private TextBox regNameField = new TextBox();
	private Label regPassLabel = new Label("Password:");
	private PasswordTextBox regPassField = new PasswordTextBox();
	private Button regButton = new Button("Register");
	
	public RegisterPage() {
		loadRegisterPage();
	}
	
	private void loadRegisterPage() {
		
		regPanel.addStyleName("regPanel");
		errorRegLabel.addStyleName("errorRegLabel");
		
		//registration page
		regHPanel1.add(regNameLabel);
		regHPanel1.add(regNameField);
		regHpanel2.add(regPassLabel);
		regHpanel2.add(regPassField);
		regHpanel2.add(regButton);
		regHpanel3.add(errorRegLabel);
		
		regPanel.add(regHPanel1);
		regPanel.add(regHpanel2);
		regPanel.add(regHpanel3);
		regPanel.setSpacing(7);
				
		//registration page stuff
		ArrayList<String> namesList = new ArrayList<String>();
		class RegisterButtonHandler implements ClickHandler{

			@Override
			public void onClick(ClickEvent event) {
				if (!regNameField.getText().matches("^[0-9a-zA-Z\\.]{1,30}$")) {
					Window.alert("'" + regNameField.getTabIndex() + "' is not a valid symbol.");
					regNameField.selectAll();
					return;
				}
				if (!regPassField.getText().matches("^[0-9a-zA-Z\\.]{1,30}$")) {
					Window.alert("'" + regPassField.getTabIndex() + "' is not a valid symbol.");
					regPassField.selectAll();
					return;
				}
				User user = new User();
				if (!namesList.contains(regNameField.getText())) {
					namesList.add(regNameField.getText());
					user.setLogin(regNameField.getText());
					user.setPassword(regPassField.getText());
					user.setRole(Role.USER);
					createUser(user);
				}	
				else {
					errorRegLabel.setText("User '" + regNameField.getText() + "' already exists");
				}
			}
			public void createUser(User user) {
				greetingService.addUserInfoToList(user.getLogin(), user.getLogin(), new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}

					@Override
					public void onSuccess(String result) {
						Window.alert("User '" + user.getLogin() + "' was successfully created");
					}
					
				});
			}
		}
		RegisterButtonHandler registerButtonHandler = new RegisterButtonHandler();
		regButton.addClickHandler(registerButtonHandler);

	}
	
	public void hideRegisterPage() {
		RootPanel.get().remove(regPanel);
	}
	public void showRegPage() {
		RootPanel.get().add(regPanel);
	}
	
}
