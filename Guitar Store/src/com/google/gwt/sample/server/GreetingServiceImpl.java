package com.google.gwt.sample.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.google.gwt.sample.client.GreetingService;
import com.google.gwt.sample.client.Guitar;
import com.google.gwt.sample.client.Role;
import com.google.gwt.sample.client.User;
import com.google.gwt.sample.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService{
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	private ArrayList<Guitar> guitarList = new ArrayList<Guitar>();
	private ArrayList<User> userList = new ArrayList<User>();
	
	
	public static void writeInFile(Guitar guitar) throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter("guitars.txt", true));
		writer.append("{ name: " + guitar.getName() + " quantity: " + guitar.getQuantity() + " type: " + 
					guitar.getType() + " color: " + guitar.getColor() + " strings: " + guitar.getStrings() + "}");
		writer.newLine();
		writer.close();
	}
    static void deleteGuitarFromFile(Guitar guitar) throws IOException{
    	File file = new File("guitars.txt");
    	
    	String delete = "{ name: " + guitar.getName() + " quantity: " + guitar.getQuantity() + " type: " + 
				guitar.getType() + " color: " + guitar.getColor() + " strings: " + guitar.getStrings() + "}";
    	File temp = File.createTempFile("file", ".txt", file.getParentFile());
    	
    	BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
    	
    	PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), "UTF-8"));
    	
    	for (String line; (line = reader.readLine()) != null;) {
    		 line = line.replace(delete, "");
    		 writer.println(line);
    	}
    	reader.close();
    	writer.close();
    	file.delete();
    	temp.renameTo(file);
    }
	static void editGuitarInFile(Guitar oldGuitar, Guitar newGuitar) throws IOException {
File file = new File("guitars.txt");
    	
    	String delete = "{ name: " + oldGuitar.getName() + " quantity: " + oldGuitar.getQuantity() + " type: " + 
				oldGuitar.getType() + " color: " + oldGuitar.getColor() + " strings: " + oldGuitar.getStrings() + "}";
    	
    	String replace = "{ name: " + newGuitar.getName() + " quantity: " + newGuitar.getQuantity() + " type: " + 
    			newGuitar.getType() + " color: " + newGuitar.getColor() + " strings: " + newGuitar.getStrings() + "}";
    	File temp = File.createTempFile("file", ".txt", file.getParentFile());
    	
    	BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
    	
    	PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), "UTF-8"));
    	
    	for (String line; (line = reader.readLine()) != null;) {
    		 line = line.replace(delete, replace);
    		 writer.println(line);
    	}
    	reader.close();
    	writer.close();
    	file.delete();
    	temp.renameTo(file);
	}
	@Override
	public Guitar newGuitarInList(Guitar guitar) throws IOException {
		writeInFile(guitar);
		guitarList.add(guitar);
		return guitar;
	}

	@Override
	public String deleteGuitarFromList(String guitarName, String guitarColor, String numberOfStrings) throws IOException {
		
		for (Guitar guitar : guitarList) {
			if (guitar.getName().equalsIgnoreCase(guitarName)
					&& guitar.getColor().equalsIgnoreCase(guitarColor)
					&& guitar.getStrings().equalsIgnoreCase(numberOfStrings)) {
				guitarList.remove(guitar);
				deleteGuitarFromFile(guitar);
				break;
			}
		}
		return "";
	}

	@Override
	public String updateGuitarInfoInsideTheList(Guitar oldGuitar, Guitar newGuitar) {
		File guitarInfo = new File("D:\\eclipse\\guitars.txt");
		try {
			File temp = File.createTempFile("guitarInfo", ".txt", guitarInfo.getParentFile());
			String charset = "UTF-8";
			String delete = "{ name: " + oldGuitar.getName() + " quantity: " + oldGuitar.getQuantity() + " type: " + 
					oldGuitar.getType() + " color: " + oldGuitar.getColor() + " strings: " + oldGuitar.getStrings() + "}";
	    	
	    	String replace = "{ name: " + newGuitar.getName() + " quantity: " + newGuitar.getQuantity() + " type: " + 
	    			newGuitar.getType() + " color: " + newGuitar.getColor() + " strings: " + newGuitar.getStrings() + "}";
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(guitarInfo), charset));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
			for (String line; (line = reader.readLine()) != null;) {
				line = line.replace(delete, replace);
				writer.println(line);
			}
			reader.close();
			writer.close();
			guitarInfo.delete();
			temp.renameTo(guitarInfo);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "";

	}

	User userInfo = new User();
	
	@Override
	public String addUserInfoToList(String userName, String userPassword) {
		userInfo.setLogin(userName);
		userInfo.setPassword(userPassword);
		userList.add(userInfo);
		return "Success";
	}

	@Override
	public String chekingIfUserExist(String userName, String userPassword) {
		User admin = new User("admin", "admin", Role.ADMIN);
		for (User userInfo : userList) {
			if (userInfo.getLogin().equals(userName) || userInfo.getLogin().equals(admin.getLogin()) && userInfo.getPassword().equals(userPassword) || userInfo.getPassword().equals(admin.getPassword())) {
				return "Welcome";
			}
		}
		return "Invalid username or password";
	}




}
